# react-native-spacex

# Install

From the app directory...

<h5>react-native run-android</h5>

# API

SpaceX open and free API

https://api.spacexdata.com/v4/launches

# Fetch request

This app populates state with data from the SpaceX API through this fetch request in components

`const call_api = () => {
        axios({
            url:'/launches',
            method: 'GET',
            timeout:3000
        }).then((res) => {dispatch({ 'type' : 'APP_DATA_DONE', 'payload' :  res.data})
            set_loading_visible(false)}).catch((err) => console.log(err))}`

# Technology used

<h5>redux</h5>





